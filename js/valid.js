$(document).ready(function() {
    var datas = JSON.parse($("#librarycontents").text());
    var bookdata = JSON.parse($("#bookcontents").text());

    getData() == null ? putData(datas) : "";
    getData1() == null ? putData1(bookdata) : "";

    var k = getData();
    var s = getData1();

    function getData() {
        return JSON.parse(localStorage.getItem("library"));
    }

    function putData(data) {
        localStorage.setItem("library", JSON.stringify(data));
    }

    function getData1() {
        return JSON.parse(localStorage.getItem("books"));
    }

    function putData1(data) {
        localStorage.setItem("books", JSON.stringify(bookdata));
    }
    var spreadsheetID1 = "1MBZozlO1R4Ym73kpjG3xn5nJgOoAh-naK2vWoBcw1Lo";
    var spreadsheetID2 = "1-leK_JqnDEcQbTJByDPLbL6sgb0fHcEg2t21UH5xJC0";
    var url1 = "https://spreadsheets.google.com/feeds/cells/" + spreadsheetID1 + "/od6/public/basic?range=A2:F5&alt=json";
    var url2 = "https://spreadsheets.google.com/feeds/cells/" + spreadsheetID2 + "/od6/public/basic?range=A2:E4&alt=json";

    $.getJSON(url1, function(data) {
        $.getJSON(url2, function(feedbacks) {
            var res = "",
                result = "";
            var j = 6;
            if (data.feed.entry[data.feed.entry.length - 6].content.$t != localStorage.getItem("latest")) {
                res += '<table border="1" align="center" style="text-align:center">' +
                    '<tr class="text-white" style="background-color:#86C232">' +
                    '<th width="50" height="60">Date</th>' +
                    '<th width="40" height="60" style="width:100px">UserName</th>' +
                    '<th width="50" height="60">Message</th>' +
                    '<th width="250" height="60">Email Id</th>' +
                    '<th width="250" height="60">Carbon Copy</th>' +
                    '<th width="120" height="60">GrantBooks</th>' +
                    '<th width="120" height="60">DeclineBooks</th>' +

                    '</tr><tr>';
                for (var i = data.feed.entry.length - 6; i < data.feed.entry.length; i++) {

                    if (i % 6 == 5) {
                        res += '<td class="d-none">' + data.feed.entry[i].content.$t + '</td>';
                    } else {
                        res += '<td width="200" height="10">' + data.feed.entry[i].content.$t + '</td>';
                    }
                    if (i % 6 == 5) {

                        res += '<td><button type="button" class="btn bttncolor btn-sm mb-4" id="try">Grant</button></td>';
                        res += '<td><button type="button" class="btn bttncolor btn-sm mb-4" id="try1">Decline</button></td>';
                        res += '</tr>';
                        res += '<tr>';
                    }

                    if (i == data.feed.entry.length - 1) {
                        res += '</tr>';
                    }
                    if (data.feed.entry.length - 6 == i) {
                        localStorage.setItem("latest", data.feed.entry[i].content.$t);
                    }
                }
                res += '</table>';

            } else {
                res += '<h3 class="text-center">No email requests for borrowing</h3>';
            }


            if (feedbacks.feed.entry.length != 0) {
                result += '<table border="1" align="center" style="text-align:center">' +
                    '<tr class="text-white" style="background-color:#86C232">' +
                    '<th width="50" height="60">Date</th>' +
                    '<th width="40" height="60" style="width:100px">UserName</th>' +
                    '<th width="50" height="60">Feedback</th>' +
                    '<th width="250" height="60">User Email Id</th>' +
                    '<th width="250" height="60">Carbon Copy</th>' +
                    '</tr><tr>';
                for (var i = 0; i < feedbacks.feed.entry.length; i++) {

                    result += '<td width="200" height="50">' + feedbacks.feed.entry[i].content.$t + '</td>';

                    if (i % 6 == 4) {
                        console.log(i);
                        result += '</tr>';
                        result += '<tr>';
                    }
                    if (i == feedbacks.feed.entry.length - 1) {
                        result += '</tr>';
                    }
                }
                result += '</table>';
            } else {
                result += '<h3 class="text-center">No email requests for borrowing</h3>';
            }


            $('#borrowmail').append(res);

            $('#feedbackmail').append(result);

        });

    });
    $(document).on("click", "#try", function() {
        grantBook();
    });
    $(document).on("click", "#try1", function() {
        declineBook();
    })

    function grantBook() {

        var ar = localStorage.getItem("array");
        console.log(ar);
        var art = ar.split(',');
        var m = art[0];
        var g = art[3];
        var h = art[4];
        // console.log(g);
        if (g == "scific") {
            console.log(s.bookgenre[0][g][m].stock);
            s.bookgenre[0][g][m].stock = s.bookgenre[0][g][m].stock - 1;
            s.bookgenre[0][g][m].status = "un-available";
            localStorage.books = JSON.stringify(s);
        } else if (g == "horror") {
            console.log(s.bookgenre[1][g][m].stock);
            s.bookgenre[1][g][m].stock = s.bookgenre[1][g][m].stock - 1;
            localStorage.books = JSON.stringify(s);

        } else if (g == "fantasy") {
            console.log(s.bookgenre[2][g][m].stock);
            s.bookgenre[2][g][m].stock = s.bookgenre[2][g][m].stock - 1;
            localStorage.books = JSON.stringify(s);

        }
        localStorage.books = JSON.stringify(s);
        console.log(" book request is accepted");
        alert("Request is granted");
    }

    function declineBook() {

        console.log(" book request is rejected");

    }


    var count = 0,
        e1 = -1,
        e2 = -1,
        t1 = -1,
        t2 = -1,
        em1 = -1,
        em2 = -1;
    var $regmail = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var $regpass = /^([0-9A-Za-z]{8,15})+$/;
    var $regname = /^([a-zA-z]{3,15})+$/;
    var $regaddress = /^[a-zA-Z0-9\s,.'-]{5,}$/;
    var $regempid = /^[0-9]{1,3}$/;

    $('#empid').on('input', function() {
        var a = $('#empid').val();
        if (a == '') {
            $('#empid').removeClass("is-valid").addClass("is-invalid");
            $('#validempid').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regempid)) {
            $('#empid').addClass("is-invalid");
            $('#validempid').removeClass("valid-feedback").addClass("invalid-feedback").text("enter only numbers");
        } else {
            $('#empid').removeClass("is-invalid").addClass("is-valid")
            $('#validempid').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
        }
    });

    $('#vfname').on('input', function() {
        var a = $('#vfname').val();
        if (a == '') {
            $('#vfname').removeClass("is-valid").addClass("is-invalid");
            $('#validfname').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regname)) {
            $('#vfname').addClass("is-invalid");
            $('#validfname').removeClass("valid-feedback").addClass("invalid-feedback").text("enter valid mail");
        } else {
            $('#vfname').removeClass("is-invalid").addClass("is-valid")
            $('#validfname').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
            localStorage.setItem("name", a);
        }
    });
    $('#vlname').on('input', function() {
        var a = $('#vlname').val();
        if (a == '') {
            $('#vlname').removeClass("is-valid").addClass("is-invalid");
            $('#validlname').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regname)) {
            $('#vlname').addClass("is-invalid");
            $('#validlname').removeClass("valid-feedback").addClass("invalid-feedback").text("enter 3-15 characters");
        } else {
            $('#vlname').removeClass("is-invalid").addClass("is-valid")
            $('#validlname').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
        }
    });

    $("#vemail").on('input', function() {
        var a = $('#vemail').val();
        if (a == '') {
            $('#vemail').removeClass("is-valid").addClass("is-invalid");
            $('#validvemail').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regmail)) {
            $('#vemail').addClass("is-invalid");
            $('#validvemail').removeClass("valid-feedback").addClass("invalid-feedback").text("enter 3-15 characters");
        } else {
            $('#vemail').removeClass("is-invalid").addClass("is-valid")
            $('#validvemail').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
            localStorage.setItem("useremail", a);
        }
    });

    $("#vpass").on('input', function() {
        var a = $('#vpass').val();

        if (a == '') {
            $('#vpass').removeClass("is-valid").addClass("is-invalid");
            $('#validvpass').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regpass)) {
            $('#vpass').addClass("is-invalid");
            $('#validvpass').removeClass("valid-feedback").addClass("invalid-feedback").text('password must match');
        } else {
            $('#vpass').removeClass("is-invalid").addClass("is-valid");
            $('#validvpass').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
            localStorage.setItem("userpassword", a);
        }
    });

    $('#vaddress').on('input', function() {
        var a = $('#vaddress').val();
        if (a == '') {
            $('#vaddress').removeClass("is-valid").addClass("is-invalid");
            $('#validaddress').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regaddress)) {
            $('#vaddress').addClass("is-invalid");
            $('#validaddress').removeClass("valid-feedback").addClass("invalid-feedback").text("enter valid address");
        } else {
            $('#vaddress').removeClass("is-invalid").addClass("is-valid")
            $('#validaddress').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
        }
    });


    var x = "",
        category = "";

    $('#cate').on('change', function() {
        x = $('#cate option:selected').val();
        console.log(x);
        category = Object.keys(k.persons[x])[0];
        console.log(category);
        var catevalue = x + ' ' + category;
        localStorage.setItem("categoryvalues", catevalue);

    });

    $("#email").on("input", function() {
        var a = $('#email').val();

        switch (x) {
            case "0":
                {
                    var adminemail = k.persons[x][category].emailid;
                    localStorage.setItem("email", adminemail);

                    checkEmail(adminemail);
                    e1 = x;
                }
                break;
            case "1":
                {
                    var teamemail = k.persons[x][category].emailid;
                    localStorage.setItem("email", teamemail);
                    checkEmail(teamemail);
                    t1 = x;
                }
                break;
            case "2":
                {

                    for (var i = 0; i < k.persons[x][category].length; i++) {
                        var dummy = k.persons[x][category][i].emailid; {
                            if (dummy == a) {
                                var employeeemail = k.persons[x][category][i].emailid;
                                localStorage.setItem("email", employeeemail);
                                em1 = i;
                                break;
                            } else {
                                continue;
                            }
                        }
                    }
                    checkEmail(employeeemail);
                }
                break;
        }

    });

    $("#pass").on("input", function() {
        var a = $("#pass").val();
        switch (x) {
            case "0":
                {
                    var adminpass = k.persons[x][category].password;
                    localStorage.setItem("password", adminpass);
                    checkPassword(adminpass);
                    e2 = x;
                }
                break;
            case "1":
                {
                    var teampass = k.persons[x][category].password;
                    localStorage.setItem("password", teampass);
                    checkPassword(teampass);
                    t2 = x;
                }
                break;
            case "2":
                {
                    for (var i = 0; i < k.persons[x][category].length; i++) {
                        var dummy = k.persons[x][category][i].password; {
                            if (dummy == a) {
                                var employeepass = k.persons[x][category][i].password;
                                localStorage.setItem("password", employeepass);
                                em2 = i;
                                break;
                            } else {
                                continue;
                            }
                        }
                    }
                    checkPassword(employeepass);
                }
                break;
        }
    });

    function checkEmail(emailid) {
        var a = $('#email').val();
        if (a == '') {
            $('#email').removeClass("is-valid").addClass("is-invalid");
            $('#validemail').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regmail) || (a != emailid)) {
            $('#email').addClass("is-invalid");
            $('#validemail').removeClass("valid-feedback").addClass("invalid-feedback").text("enter valid mail");
        } else {
            $('#email').removeClass("is-invalid").addClass("is-valid")
            $('#validemail').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");

        }
    }

    function checkPassword(passwo) {
        var a = $('#pass').val();

        if (a == '') {
            $('#pass').removeClass("is-valid").addClass("is-invalid");
            $('#validpass').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regpass) || (passwo != a)) {
            $('#pass').addClass("is-invalid");
            $('#validpass').removeClass("valid-feedback").addClass("invalid-feedback").text('password must match');
        } else {
            $('#pass').removeClass("is-invalid").addClass("is-valid");
            $('#validpass').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
        }
    }
    $("#cemail").on("input", function() {
        var count1 = 0;
        var a = $('#cemail').val();
        var localmail = localStorage.getItem("email");
        if (a == '') {
            $('#cemail').removeClass("is-valid").addClass("is-invalid");
            $('#validnewemail').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regmail) || (a != localmail)) {
            $('#cemail').addClass("is-invalid");
            $('#validnewemail').removeClass("valid-feedback").addClass("invalid-feedback").text("enter valid mail");
        } else {
            $('#cemail').removeClass("is-invalid").addClass("is-valid")
            $('#validnewemail').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
            count1++;
            console.log(count1);
        }
    });
    $("#oldpass").on("input", function() {
        var count2 = 0;
        var a = $("#oldpass").val();
        var oldpassword = localStorage.getItem("password");

        if (a == '') {
            console.log("inside pass empty");

            $('#oldpass').removeClass("is-valid").addClass("is-invalid");
            $('#validoldpass').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (a != oldpassword) {
            $('#oldpass').addClass("is-invalid");
            $('#validoldpass').removeClass("valid-feedback").addClass("invalid-feedback").text('password must match');
        } else {
            $('#oldpass').removeClass("is-invalid").addClass("is-valid");
            $('#validoldpass').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
            count2++;
            console.log(count2);
        }
    });
    $("#changepass").on("input", function() {
        var count3 = 0;
        var a = $("#changepass").val();
        if (a == '') {
            $('#changepass').removeClass("is-valid").addClass("is-invalid");
            $('#validpass').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if (!a.match($regpass)) {
            $('#changepass').addClass("is-invalid");
            $('#validpass').removeClass("valid-feedback").addClass("invalid-feedback").text('password must contain atleat 1 capital letter,1 small letter and 1 number with length 8-15');
        } else {
            $('#changepass').removeClass("is-invalid").addClass("is-valid");
            $('#validpass').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
            localStorage.setItem("newpass", a);
            count3++;
            console.log(count3);
        }
    });
    $("#cpass").on("input", function() {
        var count4 = 0;
        var a = $("#cpass").val();
        var localpass = localStorage.getItem("newpass");
        if (a == '') {
            $('#cpass').removeClass("is-valid").addClass("is-invalid");
            $('#validcpass').removeClass("valid-feedback").addClass("invalid-feedback").text('field is required');
        }
        if ((localpass != a)) {
            $('#cpass').addClass("is-invalid");
            $('#validcpass').removeClass("valid-feedback").addClass("invalid-feedback").text('password must match');
        } else {
            $('#cpass').removeClass("is-invalid").addClass("is-valid");
            $('#validcpass').removeClass("invalid-feedback").addClass("valid-feedback").text("correct");
            count4++;
            console.log(count4);
        }
    });

    function setPassword(setpass) {
        var b = localStorage.getItem("categoryvalues");

        console.log(b[0] + b[1]);
        var cvalue1 = b[0];
        var cvalue2 = b[1];
        switch (b[0]) {
            case "0":
                {
                    k.persons[0].admin.password = setpass;
                    localStorage.library = JSON.stringify(k);
                }
                break;
            case "1":
                {
                    k.persons[1].teamlead.password = setpass;
                    localStorage.library = JSON.stringify(k);
                }
                break;
            case "2":
                {
                    for (var i = 0; i < k.persons[2].employee.length; i++) {
                        k.persons[2].employee[i].password = setpass;
                        localStorage.library = JSON.stringify(k);
                    }
                }
                break;
        }
    }
    var user = "";
    $('#sub').on('click', function() {
        if (e1 == 0 && e2 == 0) {
            localStorage.setItem("current", "admin");
            $('a').attr('href', 'admin.html');
        } else if (t1 == 1 && t2 == 1) {
            localStorage.setItem("current", "team");

            $('a').attr('href', 'team.html');
        } else if (em1 != -1 && em2 != -1) {
            localStorage.setItem("current", "user");

            for (var i = 0; i < k.persons[x][category].length; i++) {
                if (em1 == i && em2 == i) {
                    user = k.persons[x][category][i].username;
                    $('a').attr('href', 'user.html');
                } else {
                    continue;
                }
            }
        } else {
            alert("Please enter login details");
        }

    });

    $('#confirmsub').on('click', function() {
        var counter = 0;
        var a = localStorage.getItem("newpass");
        setPassword(a);

        for (var i = 1; i < 5; i++) {
            if ((count + i) > 0) {
                counter++;
            } else {
                continue;
            }
        }

        if (counter == 4) {
            console.log("login");

            $('a').attr('href', 'login.html');
        } else {
            alert("Please fill all the fields");
        }
    });

    $('#regsubmit').on('click', function() {
        var usermail = localStorage.getItem("useremail");
        var userpass = localStorage.getItem("userpassword");
        var username = localStorage.getItem("name")
        var l = k.persons[2].employee.length;
        console.log(k.persons[2].employee.length);

        var newarr = {
            name: username,
            emailid: usermail,
            password: userpass
        };
        k.persons[2].employee[l] = newarr;
        localStorage.library = JSON.stringify(k);
    });

    var add1 = '<div class="form-group pl-3 py-3">' +
        '<label for="Select1">Select category</label>' +
        '<select class="form-control" id="bookcate" style="width:200px">' +
        '<option>Select Category</option>';

    for (var i = 0; i < s.bookgenre.length; i++) {
        // console.log(Object.keys(s.bookgenre[i])[0]);
        add1 += '           <option value="' + i + '">' + Object.keys(s.bookgenre[i])[0] + '</option>';
    }

    add1 += '</select>';

    $('#displaycate').append(add1);

    var showbooks = '<table align="center" border="1" class=" table table-striped c" style="text-align:center">' +
        '       <tr class="text-white" style="background-color:#86C232">' +
        '       <th class="mt-3" height=70 width=40>ID</th>' +
        '       <th width=40>Name</th>' +
        '       <th width=40>Author</th>' +
        '       <th width=40>Category</th>' +
        '       <th width=40>Stock</th>' +
        '' +
        '   </tr>';
    for (var i = 0; i < s.bookgenre.length; i++) {
        for (var j = 0; j < s.bookgenre[i][Object.keys(s.bookgenre[i])[0]].length; j++) {
            if (s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].id != undefined)

            {
                showbooks += '   <tr>' +
                    '       <td height=30 id="bookid">' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].id + '</td>' +
                    '       <td id="bookname">' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].name + '</td>' +
                    '       <td id="author">' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].author + '</td>' +
                    '       <td id="category">' + Object.keys(s.bookgenre[i])[0] + '</td>' +
                    '       <td id="stock">' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].stock + '</td>' +
                    '   </tr>';
            }
        }
    }
    showbooks += '</table>';
    $('#displaybooks').append(showbooks);


    $('#bookcate').on('change', function() {
        console.log("in cate");
        y = $('#bookcate option:selected').val();
        console.log(y);
        category = Object.keys(s.bookgenre[y])[0];
        console.log(category);
        var dispbooks = '';
        $('.c').remove();
        dispbooks += '<table align="center" border="1" class="table table-striped c my-4" style="text-align:center">' +
            '       <tr class="text-white" style="background-color:#86C232">' +
            '       <th height=70 width=40>ID</th>' +
            '       <th width=40>Name</th>' +
            '       <th width=40>Author</th>' +
            '       <th width=40>Category</th>' +
            '       <th width=40>Stock</th>' +
            '' +
            '   </tr>';
        for (var i = 0; i < s.bookgenre[y][category].length; i++) {
            if (s.bookgenre[y][category][i].id != undefined) {
                dispbooks += '   <tr>' +
                    '       <td height=30 id="bookid">' + s.bookgenre[y][category][i].id + '</td>' +
                    '       <td id="bookname">' + s.bookgenre[y][category][i].name + '</td>' +
                    '       <td id="author">' + s.bookgenre[y][category][i].author + '</td>' +
                    '       <td id="category">' + category + '</td>' +
                    '       <td id="stock">' + s.bookgenre[y][category][i].stock + '</td>' +
                    '   </tr>';
            }
        }
        dispbooks += '</table></div>';

        $('#displaybooks').append(dispbooks);
    });
    var add2 = '<div class="form-group pl-3 py-3">' +
        '<label for="Select1">Select category</label>' +
        '<select class="form-control" id="bookcate1" style="width:200px">' +
        '<option>Select Category</option>';

    for (var i = 0; i < s.bookgenre.length; i++) {
        add2 += '           <option value="' + i + '">' + Object.keys(s.bookgenre[i])[0] + '</option>';
    }

    add2 += '</select>';
    $('#displaycate1').append(add2);

    var showbooks1 = '<table align="center" border="1" class="table table-striped c" style="text-align:center">' +
        '       <tr class="text-white" style="background-color:#86C232">' +
        '       <th height=70 width=40>ID</th>' +
        '       <th width=40>Name</th>' +
        '       <th width=40>Author</th>' +
        '       <th width=40>Category</th>' +
        '       <th width=40>Stock</th>' +
        '       <th width=40>Status</th>' +
        '       <th width=40>Borrow</th>' +
        '' +
        '   </tr>';
    for (var i = 0; i < s.bookgenre.length; i++) {

        for (var j = 0; j < s.bookgenre[i][Object.keys(s.bookgenre[i])[0]].length; j++) {
            if (s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].id != undefined)

            {
                showbooks1 += '   <tr>' +
                    '       <td height=30 class=' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].id + '>' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].id + '</td>' +
                    '       <td id="bookname">' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].name + '</td>' +
                    '       <td id="author">' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].author + '</td>' +
                    '       <td id="category">' + ' ' + Object.keys(s.bookgenre[i])[0] + '</td>' +
                    '       <td id="stock">' + ' ' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].stock + ' ' + '</td>' +
                    '       <td id="status">' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].status + '</td>' +
                    '       <td><a class="btn text-white bttncolor btn-sm my-3" id=' + j + ' href="emails.html">Borrow</a></td>' +
                    '   </tr>';

            }
        }
    }
    showbooks1 += '</table>';
    $('#displaybooks1').append(showbooks1);

    $('#bookcate1').on('change', function() {
        y = $('#bookcate1 option:selected').val();
        category = Object.keys(s.bookgenre[y])[0];
        var dispbooks = '';
        $('.c').remove();
        dispbooks += '<table align="center" border="1" class="table table-striped c" style="text-align:center" my-4">' +
            '       <tr class="text-white" style="background-color:#86C232">' +
            '       <th height=70 width=40>ID</th>' +
            '       <th width=60>Name</th>' +
            '       <th width=60>Author</th>' +
            '       <th width=40>Category</th>' +
            '       <th width=40>Stock</th>' +
            '       <th width=40>Status</th>' +
            '       <th width=40>Borrow</th>' +
            '' +
            '   </tr>';
        for (var i = 0; i < s.bookgenre[y][category].length; i++) {
            if (s.bookgenre[y][category][i].id != undefined) {
                dispbooks += '   <tr>' +
                    '       <td height=30 class=' + s.bookgenre[y][category][i].id + '>' + s.bookgenre[y][category][i].id + '</td>' +
                    '       <td id="bookname">' + s.bookgenre[y][category][i].name + '</td>' +
                    '       <td id="author">' + s.bookgenre[y][category][i].author + '</td>' +
                    '       <td id="category">' + category + '</td>' +
                    '       <td id="stock">' + ' ' + s.bookgenre[y][category][i].stock + ' ' + '</td>' +
                    '       <td id="status">' + s.bookgenre[i][Object.keys(s.bookgenre[i])[0]][j].status + '</td>' +
                    '       <td><a class="btn text-white bttncolor btn-sm my-3" href="emails.html">Borrow</a></td>' +
                    '   </tr>';
            }
        }
        dispbooks += '</table></div>';

        $('#displaybooks1').append(dispbooks);
    });


    var arr = [],
        i = 0,
        uservalue = [];

    $(document).on('click', '#displaybooks1 table tr td a', function() {
        console.log($(this).parents("tr").children().text());
        $(this).parents("tr").children().each(function() {
            console.log(this.innerText);
            arr[i] = this.innerText;
            i++;
        });
        for (var i = 0; i < arr.length; i++) {
            if (i < 10) {
                continue;
            } else {
                uservalue[j] = arr[i];
                j++;
            }
        }
        console.log(uservalue);
        localStorage.setItem("array", uservalue);
        // console.log(arr);
    });
    $(document).on('click', '.signout', function() {
        localStorage.setItem("current", "0");
        var curr = localStorage.getItem("current");
        if (curr == 0) {
            $('a').attr('href', 'login.html');

        }
    });



    var add = '<div class="form-group pl-3 py-3">' +
        '<label for="Select1">Select category</label>' +
        '<select class="form-control" id="addnew" style="width:200px">' +
        '<option></option>';

    for (var i = 0; i < s.bookgenre.length; i++) {
        add += '           <option value="' + i + '">' + Object.keys(s.bookgenre[i])[0] + '</option>';
    }

    add += '</select>';

    $('#selectcate').append(add);

    $(document).on('click', '#addcate', function() {
        var inputval = $('#cateinput').val();
        console.log($.isNumeric(inputval));
        if (inputval == "") {
            alert("enter category to add");
        } else if ($.isNumeric(inputval)) {
            alert("enter alphabets only");
        } else {
            var newcategory = {
                [inputval]: [{}]
            };
            var flag = 0;
            for (var i = 0; i < s.bookgenre.length; i++) {
                if (inputval == Object.keys(s.bookgenre[i])[0]) {
                    flag = 1;
                    alert("category already exists");
                } else {
                    continue;
                }
            }
            if (flag == 0) {
                s.bookgenre.push(newcategory);
                localStorage.books = JSON.stringify(s);
                alert("category added successfully");
            }
        }
    });

    $(document).on('click', '#addsub', function() {
        var xy = $('#addnew option:selected').val();
        xcategory = Object.keys(s.bookgenre[xy])[0];
        var formelements = [];
        $('#form2 input, #form2 textarea').each(
            function(index) {
                var input = $(this);
                formelements.push(input.val());
            }
        );
        var array1 = {
            id: parseInt(formelements[0]),
            name: formelements[1],
            author: formelements[2],
            desc: formelements[4],
            nop: formelements[3] + "pages",
            publish: formelements[5],
            imgpath: "",
            stock: parseInt(formelements[6]),
            status: formelements[7]
        };
        s.bookgenre[xy][xcategory].push(array1);
        localStorage.books = JSON.stringify(s);
        $('a').attr('href', 'admin.html');
    });

    $('#cusDropdown a').on('click', function() {
        $($(this).attr("href")).slideToggle("show");
    });

    $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("table tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

});